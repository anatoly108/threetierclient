﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace threetierClient.client
{
    public interface IThreetierController
    {

        void NewAppointment();
        void CancelAppointment();
        void ResumeAppointment();

        void NewDoctor();
        void NewPatient();
        void NewDept();
    }
}
