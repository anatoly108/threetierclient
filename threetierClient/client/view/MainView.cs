﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using threetierClient.client;
using threetierClient.client.view;
using threetierClient.threetier;

namespace threetierClient
{
    public partial class MainView : Form
    {
        private IThreetierController threetierController;
        private Management managementView;

        public MainView(IThreetierController threetierController)
        {
            this.threetierController = threetierController;
            InitializeComponent();
        }

        private void newAppointmentButton_Click(object sender, EventArgs e)
        {
            // Вызываем так, будто нет никакого сервера
            threetierController.NewAppointment();
        }

        private void resumeAppointmentButton_Click(object sender, EventArgs e)
        {
            threetierController.ResumeAppointment();
        }

        private void cancelAppointmentButton_Click(object sender, EventArgs e)
        {
            threetierController.CancelAppointment();
        }

        private void Search(object sender, EventArgs e)
        {
            
            threetierController.Search();
        }

        private void перейтиВРежимКурсовойБДToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void новыйПациентToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Management managementForm = new Management(threetierController);
            this.ManagementView = managementForm;
            managementForm.Show();
        }

        public Management ManagementView
        {
            get { return managementView; }
            set { managementView = value; }
        }
    }
}
