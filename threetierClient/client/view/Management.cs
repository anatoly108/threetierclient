﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace threetierClient.client.view
{
    public partial class Management : Form
    {
        private IThreetierController threetierController;

        public Management(IThreetierController threetierController)
        {
            // TODO: Предусмотреть в конструкторе в параметре IThreetierController или IThreetierBdController
            // TODO: И, собственно, создать IThreetierBdController
            InitializeComponent();
            this.threetierController = threetierController;
            this.UpdateControls();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void newDoctor_Click(object sender, EventArgs e)
        {
            threetierController.NewDoctor();
        }

        private void newDept_Click(object sender, EventArgs e)
        {
            threetierController.NewDept();
        }

        private void newPatient_Click(object sender, EventArgs e)
        {
            threetierController.NewPatient();
        }

        public void UpdateControls()
        {
            this.Doctors.Rows.Clear();
            this.Patients.Rows.Clear();
            this.Depts.Rows.Clear();

            XmlDocument doctorXml = new XmlDocument();
            XmlDocument patientXml = new XmlDocument();
            XmlDocument deptXml = new XmlDocument();

            // doctor, patient и т. д. соответствуют названиям таблиц

            // Создаём пустой xml с докторами (пустой xml означает, что надо вытащить все записи)
            StringWriter xml = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("doctor");
            //xmlWriter.WriteElementString("Volume", XmlConvert.ToString(10));
            xmlWriter.WriteEndElement();
            doctorXml.LoadXml(xml.ToString());

            // С пациентами
            xml = new StringWriter();
            xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("patient");
            xmlWriter.WriteEndElement();
            patientXml.LoadXml(xml.ToString());

            xml = new StringWriter();
            xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("department");
            xmlWriter.WriteEndElement();
            deptXml.LoadXml(xml.ToString());

            XmlDocument doctors = threetierController.GetData(doctorXml);
            XmlDocument patients = threetierController.GetData(patientXml);
            XmlDocument depts = threetierController.GetData(deptXml);

            this.newPatientHomeDocComboBox.Items.Clear();
            this.newPatientHomeDocComboBox.Items.AddRange(this.GetValuesFromXml(doctors.OuterXml, "doctor_name").ToArray());
            if (this.newPatientHomeDocComboBox.Items.Count > 0)
                this.newPatientHomeDocComboBox.SelectedIndex = 0;

            this.newDoctorDeptComboBox.Items.Clear();
            this.newDoctorDeptComboBox.Items.AddRange(this.GetValuesFromXml(depts.OuterXml, "dept_name").ToArray());
            if (this.newDoctorDeptComboBox.Items.Count > 0)
                this.newDoctorDeptComboBox.SelectedIndex = 0;

            /**
             * Подготавливаем коллекции пациентов, докторов, отделов
             */

            ArrayList patientId = this.GetValuesFromXml(patients.OuterXml, "patient_id");
            ArrayList patientName = this.GetValuesFromXml(patients.OuterXml, "patient_name");
            ArrayList patientHomeDoctor = this.GetValuesFromXml(patients.OuterXml, "patient_home_doctor_id");

            ArrayList doctorId = this.GetValuesFromXml(doctors.OuterXml, "doctor_id");
            ArrayList doctorNameStr = this.GetValuesFromXml(doctors.OuterXml, "doctor_name");
            ArrayList doctorDept = this.GetValuesFromXml(doctors.OuterXml, "doctor_dept_id");

            ArrayList deptId = this.GetValuesFromXml(depts.OuterXml, "dept_id");
            ArrayList deptName = this.GetValuesFromXml(depts.OuterXml, "dept_name");

            /**
             * Пациенты
             */
            for (int i = 0; i < patientId.Count; i++)
            {
                String id = (String) patientId[i];
                String name = (String) patientName[i];
                String homeDoctorId = (String) patientHomeDoctor[i];

                xml = new StringWriter();
                xmlWriter = new XmlTextWriter(xml);
                xmlWriter.WriteStartElement("doctor");
                xmlWriter.WriteElementString("doctor_id", homeDoctorId);
                xmlWriter.WriteEndElement();
                doctorXml.LoadXml(xml.ToString());
                XmlDocument doctor = threetierController.GetData(doctorXml);
                String doctorNamePatient = this.GetValuesFromXml(doctor.OuterXml, "doctor_name")[0].ToString();

                String[] row = { id, name, doctorNamePatient };
                this.Patients.Rows.Add(row);
            }

            for (int i = 0; i < doctorId.Count; i++)
            {
                String id = (String) doctorId[i];
                String name = (String) doctorNameStr[i];
                String doctorDeptId = (String) doctorDept[i];

                xml = new StringWriter();
                xmlWriter = new XmlTextWriter(xml);
                xmlWriter.WriteStartElement("department");
                xmlWriter.WriteElementString("dept_id", doctorDeptId);
                xmlWriter.WriteEndElement();
                doctorXml.LoadXml(xml.ToString());
                XmlDocument doctor = threetierController.GetData(doctorXml);
                String departmentName = this.GetValuesFromXml(doctor.OuterXml, "dept_name")[0].ToString();

                String[] row = { id, name, departmentName };
                this.Doctors.Rows.Add(row);
            }

            for (int i = 0; i < deptId.Count; i++)
            {
                String id = (String) deptId[i];
                String name = (String) deptName[i];

                String[] row = {id, name};
                this.Depts.Rows.Add(row);
                
            }
        }

        public ArrayList GetValuesFromXml(String xmlString, String valueName)
        {
            ArrayList values = new ArrayList();

            XmlReader reader = XmlReader.Create(new System.IO.StringReader(xmlString));
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {

                    if (!reader.IsEmptyElement)
                    {
                        if (reader.Name == valueName)
                        {
                            reader.Read();
                            values.Add(reader.ReadString());
                        }
                    }
                }
            }

            return values;
        }

        public IContainer Components
        {
            get { return components; }
            set { components = value; }
        }

        public IThreetierController ThreetierController
        {
            get { return threetierController; }
            set { threetierController = value; }
        }

        public TabControl TabControl1
        {
            get { return tabControl1; }
            set { tabControl1 = value; }
        }

        public TabPage TabPage1
        {
            get { return tabPage1; }
            set { tabPage1 = value; }
        }

        public TabPage TabPage3
        {
            get { return tabPage3; }
            set { tabPage3 = value; }
        }

        public DataGridView Doctors1
        {
            get { return Doctors; }
            set { Doctors = value; }
        }

        public TabPage TabPage2
        {
            get { return tabPage2; }
            set { tabPage2 = value; }
        }

        public DataGridViewTextBoxColumn DoctorIndex
        {
            get { return doctorIndex; }
            set { doctorIndex = value; }
        }

        public DataGridViewTextBoxColumn DoctorDepartment
        {
            get { return doctorDepartment; }
            set { doctorDepartment = value; }
        }

        public Panel Panel1
        {
            get { return panel1; }
            set { panel1 = value; }
        }

        public DataGridViewTextBoxColumn DoctorName
        {
            get { return doctorName; }
            set { doctorName = value; }
        }

        public TextBox NewDoctorNameTextBox
        {
            get { return newDoctorNameTextBox; }
            set { newDoctorNameTextBox = value; }
        }

        public Label Label2
        {
            get { return label2; }
            set { label2 = value; }
        }

        public Label Label1
        {
            get { return label1; }
            set { label1 = value; }
        }

        public Panel Panel2
        {
            get { return panel2; }
            set { panel2 = value; }
        }

        public DataGridView Patients1
        {
            get { return Patients; }
            set { Patients = value; }
        }

        public DataGridViewTextBoxColumn DataGridViewTextBoxColumn1
        {
            get { return dataGridViewTextBoxColumn1; }
            set { dataGridViewTextBoxColumn1 = value; }
        }

        public DataGridViewTextBoxColumn DataGridViewTextBoxColumn2
        {
            get { return dataGridViewTextBoxColumn2; }
            set { dataGridViewTextBoxColumn2 = value; }
        }

        public DataGridViewTextBoxColumn DataGridViewTextBoxColumn3
        {
            get { return dataGridViewTextBoxColumn3; }
            set { dataGridViewTextBoxColumn3 = value; }
        }

        public Panel Panel3
        {
            get { return panel3; }
            set { panel3 = value; }
        }

        public DataGridView Depts1
        {
            get { return Depts; }
            set { Depts = value; }
        }

        public DataGridViewTextBoxColumn DataGridViewTextBoxColumn4
        {
            get { return dataGridViewTextBoxColumn4; }
            set { dataGridViewTextBoxColumn4 = value; }
        }

        public DataGridViewTextBoxColumn DataGridViewTextBoxColumn5
        {
            get { return dataGridViewTextBoxColumn5; }
            set { dataGridViewTextBoxColumn5 = value; }
        }

        public ComboBox NewDoctorDeptComboBox
        {
            get { return newDoctorDeptComboBox; }
            set { newDoctorDeptComboBox = value; }
        }

        public ComboBox NewPatientHomeDocComboBox
        {
            get { return newPatientHomeDocComboBox; }
            set { newPatientHomeDocComboBox = value; }
        }

        public TextBox NewPatientNameTextBox
        {
            get { return newPatientNameTextBox; }
            set { newPatientNameTextBox = value; }
        }

        public TextBox NewDoctorNameTextBox1
        {
            get { return newDoctorNameTextBox; }
            set { newDoctorNameTextBox = value; }
        }

        public ComboBox NewDoctorDeptComboBox1
        {
            get { return newDoctorDeptComboBox; }
            set { newDoctorDeptComboBox = value; }
        }

        public ComboBox NewPatientHomeDocComboBox1
        {
            get { return newPatientHomeDocComboBox; }
            set { newPatientHomeDocComboBox = value; }
        }

        public TextBox NewPatientNameTextBox1
        {
            get { return newPatientNameTextBox; }
            set { newPatientNameTextBox = value; }
        }

        public TextBox NewDeptNameTextBox
        {
            get { return newDeptNameTextBox; }
            set { newDeptNameTextBox = value; }
        }
    }
}
