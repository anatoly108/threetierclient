﻿using System.Windows.Forms;

namespace threetierClient
{
    partial class MainView
    {

        private System.Windows.Forms.DataGridView appointmentTable;
        private System.Windows.Forms.Button cancelAppointmentButton;
        private System.Windows.Forms.Button resumeAppointmentButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker appointmentDate;
        private System.Windows.Forms.ComboBox DoctorComboBox;
        private System.Windows.Forms.ComboBox patientComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker appointmentTime;
        private System.Windows.Forms.Button newAppointmentButton;
        private System.Windows.Forms.Panel panel1;
        
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.appointmentTable = new System.Windows.Forms.DataGridView();
            this.VisitNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Doctor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Parient = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cancelAppointmentButton = new System.Windows.Forms.Button();
            this.resumeAppointmentButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.newAppointmentButton = new System.Windows.Forms.Button();
            this.appointmentTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.appointmentDate = new System.Windows.Forms.DateTimePicker();
            this.DoctorComboBox = new System.Windows.Forms.ComboBox();
            this.patientComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cleverSearch = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.действияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новыйПациентToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.firstTaskDoctorsCombo = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentTable)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // appointmentTable
            // 
            this.appointmentTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.appointmentTable.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.appointmentTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.appointmentTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VisitNumber,
            this.Doctor,
            this.Parient,
            this.Data});
            this.appointmentTable.Location = new System.Drawing.Point(0, 64);
            this.appointmentTable.Name = "appointmentTable";
            this.appointmentTable.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.appointmentTable.Size = new System.Drawing.Size(543, 342);
            this.appointmentTable.TabIndex = 0;
            // 
            // VisitNumber
            // 
            this.VisitNumber.HeaderText = "Номер посещения";
            this.VisitNumber.Name = "VisitNumber";
            this.VisitNumber.ReadOnly = true;
            // 
            // Doctor
            // 
            this.Doctor.HeaderText = "Доктор";
            this.Doctor.Name = "Doctor";
            this.Doctor.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Parient
            // 
            this.Parient.HeaderText = "Пациент";
            this.Parient.Name = "Parient";
            this.Parient.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Parient.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Data
            // 
            this.Data.HeaderText = "Дата";
            this.Data.Name = "Data";
            // 
            // cancelAppointmentButton
            // 
            this.cancelAppointmentButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.cancelAppointmentButton.Location = new System.Drawing.Point(10, 23);
            this.cancelAppointmentButton.Name = "cancelAppointmentButton";
            this.cancelAppointmentButton.Size = new System.Drawing.Size(284, 30);
            this.cancelAppointmentButton.TabIndex = 0;
            this.cancelAppointmentButton.Text = "Отменить приём";
            this.cancelAppointmentButton.UseVisualStyleBackColor = true;
            this.cancelAppointmentButton.Click += new System.EventHandler(this.cancelAppointmentButton_Click);
            // 
            // resumeAppointmentButton
            // 
            this.resumeAppointmentButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.resumeAppointmentButton.Location = new System.Drawing.Point(10, 53);
            this.resumeAppointmentButton.Name = "resumeAppointmentButton";
            this.resumeAppointmentButton.Size = new System.Drawing.Size(284, 30);
            this.resumeAppointmentButton.TabIndex = 1;
            this.resumeAppointmentButton.Text = "Возобновить приём";
            this.resumeAppointmentButton.UseVisualStyleBackColor = true;
            this.resumeAppointmentButton.Click += new System.EventHandler(this.resumeAppointmentButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.resumeAppointmentButton);
            this.groupBox1.Controls.Add(this.cancelAppointmentButton);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox1.Size = new System.Drawing.Size(304, 99);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Работа с таблицей";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.newAppointmentButton);
            this.groupBox2.Controls.Add(this.appointmentTime);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.appointmentDate);
            this.groupBox2.Controls.Add(this.DoctorComboBox);
            this.groupBox2.Controls.Add(this.patientComboBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(10, 179);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox2.Size = new System.Drawing.Size(301, 150);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Запись на приём";
            // 
            // newAppointmentButton
            // 
            this.newAppointmentButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.newAppointmentButton.Location = new System.Drawing.Point(10, 117);
            this.newAppointmentButton.Name = "newAppointmentButton";
            this.newAppointmentButton.Size = new System.Drawing.Size(281, 23);
            this.newAppointmentButton.TabIndex = 8;
            this.newAppointmentButton.Text = "Записать";
            this.newAppointmentButton.UseVisualStyleBackColor = true;
            this.newAppointmentButton.Click += new System.EventHandler(this.newAppointmentButton_Click);
            // 
            // appointmentTime
            // 
            this.appointmentTime.CustomFormat = "HH:mm";
            this.appointmentTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.appointmentTime.Location = new System.Drawing.Point(68, 81);
            this.appointmentTime.Name = "appointmentTime";
            this.appointmentTime.ShowUpDown = true;
            this.appointmentTime.Size = new System.Drawing.Size(62, 20);
            this.appointmentTime.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Время";
            // 
            // appointmentDate
            // 
            this.appointmentDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.appointmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.appointmentDate.Location = new System.Drawing.Point(152, 81);
            this.appointmentDate.Name = "appointmentDate";
            this.appointmentDate.Size = new System.Drawing.Size(135, 20);
            this.appointmentDate.TabIndex = 5;
            this.appointmentDate.Value = new System.DateTime(2013, 5, 26, 22, 41, 49, 0);
            // 
            // DoctorComboBox
            // 
            this.DoctorComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DoctorComboBox.FormattingEnabled = true;
            this.DoctorComboBox.Location = new System.Drawing.Point(68, 49);
            this.DoctorComboBox.Name = "DoctorComboBox";
            this.DoctorComboBox.Size = new System.Drawing.Size(219, 21);
            this.DoctorComboBox.TabIndex = 3;
            // 
            // patientComboBox
            // 
            this.patientComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.patientComboBox.FormattingEnabled = true;
            this.patientComboBox.Location = new System.Drawing.Point(68, 22);
            this.patientComboBox.Name = "patientComboBox";
            this.patientComboBox.Size = new System.Drawing.Size(219, 21);
            this.patientComboBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Врач";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Пациент";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10, 10, 10, 20);
            this.panel1.Size = new System.Drawing.Size(324, 359);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Location = new System.Drawing.Point(4, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(345, 24);
            this.panel2.TabIndex = 4;
            // 
            // cleverSearch
            // 
            this.cleverSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cleverSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cleverSearch.Location = new System.Drawing.Point(3, 32);
            this.cleverSearch.Name = "cleverSearch";
            this.cleverSearch.Size = new System.Drawing.Size(540, 24);
            this.cleverSearch.TabIndex = 0;
            this.cleverSearch.Text = "Умный поиск";
            this.cleverSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Search);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(549, 32);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(353, 374);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(345, 348);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Посещения";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.comboBox1);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.firstTaskDoctorsCombo);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(345, 348);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Курсовая БД";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.действияToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(914, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // действияToolStripMenuItem
            // 
            this.действияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новыйПациентToolStripMenuItem});
            this.действияToolStripMenuItem.Name = "действияToolStripMenuItem";
            this.действияToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.действияToolStripMenuItem.Text = "Действия";
            // 
            // новыйПациентToolStripMenuItem
            // 
            this.новыйПациентToolStripMenuItem.Name = "новыйПациентToolStripMenuItem";
            this.новыйПациентToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.новыйПациентToolStripMenuItem.Text = "Управление пациентами и докторами";
            this.новыйПациентToolStripMenuItem.Click += new System.EventHandler(this.новыйПациентToolStripMenuItem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Все посещения пациентов отдела: ";
            // 
            // firstTaskDoctorsCombo
            // 
            this.firstTaskDoctorsCombo.FormattingEnabled = true;
            this.firstTaskDoctorsCombo.Location = new System.Drawing.Point(198, 9);
            this.firstTaskDoctorsCombo.Name = "firstTaskDoctorsCombo";
            this.firstTaskDoctorsCombo.Size = new System.Drawing.Size(121, 21);
            this.firstTaskDoctorsCombo.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(117, 72);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Запустить";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "у которых домашний доктор:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(198, 40);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 4;
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 409);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.cleverSearch);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.appointmentTable);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainView";
            this.Text = "Регистратура поликлиники";
            ((System.ComponentModel.ISupportInitialize)(this.appointmentTable)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataGridViewTextBoxColumn VisitNumber;
        private DataGridViewTextBoxColumn Doctor;
        private DataGridViewTextBoxColumn Parient;
        private DataGridViewTextBoxColumn Data;
        private Panel panel2;
        private TextBox cleverSearch;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem действияToolStripMenuItem;
        private ToolStripMenuItem новыйПациентToolStripMenuItem;
        private Label label4;
        private ComboBox comboBox1;
        private Label label5;
        private Button button1;
        private ComboBox firstTaskDoctorsCombo;



        public DataGridView PatientTable
        {
            get { return appointmentTable; }
        }

        public Button CancelAppointmentButton
        {
            get { return cancelAppointmentButton; }
        }

        public Button ResumeAppointmentButton
        {
            get { return resumeAppointmentButton; }
        }

        public DateTimePicker AppointmentDate
        {
            get { return appointmentDate; }
        }

        public ComboBox DoctorComboBox1
        {
            get { return DoctorComboBox; }
        }

        public ComboBox PatientComboBox
        {
            get { return patientComboBox; }
        }

        public DateTimePicker AppointmentTime
        {
            get { return appointmentTime; }
        }

        public Button NewAppointmentButton
        {
            get { return newAppointmentButton; }
        }

        public DataGridViewTextBoxColumn VisitNumber1
        {
            get { return VisitNumber; }
        }

        public DataGridViewTextBoxColumn Data1
        {
            get { return Data; }
        }

        public DataGridView AppointmentTable
        {
            get { return appointmentTable; }
        }

        public TextBox CleverSearch
        {
            get { return cleverSearch; }
        }
    }
}

