﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace threetierClient.client
{
    public interface IThreetierController
    {

        void NewAppointment();
        void CancelAppointment();
        void ResumeAppointment();

        void NewDoctor();
        void NewPatient();
        void NewDept();

        void Search();

        // Метод обновляет все элементы упаравления в соответствии с данными из базы
        void UpdateControls();

        // Метод выполняет выборку из БД в соответствии с файлом XML
        XmlDocument GetData(XmlDocument xmlDocument, bool like = false);
    }
}
