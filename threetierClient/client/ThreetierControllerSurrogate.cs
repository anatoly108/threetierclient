﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using threetierClient.client;
using threetierClient.client.view;
using threetierServer.server;

namespace threetierClient.threetier
{
    class ThreetierControllerSurrogate : IThreetierController
    {
      
        private MainView mainView;
        private TcpClient tcpClient;
        private NetworkStream writerStream;
        private Management managementView;

        public ThreetierControllerSurrogate(int port)
        {
            this.mainView = new MainView(this);
            

            string message_str = "";

            IPAddress ipAddress = Dns.Resolve("localhost").AddressList[0];
            tcpClient = new TcpClient(ipAddress.ToString(), port);
            writerStream = tcpClient.GetStream();

            UpdateControls();

            Application.Run(this.mainView);
           
        }


        public void NewAppointment()
        {
            String doctor = this.mainView.DoctorComboBox1.SelectedItem.ToString();
            String patient = this.mainView.PatientComboBox.SelectedItem.ToString();
            String date = this.mainView.AppointmentTime.Text + " " + this.mainView.AppointmentDate.Text;

            XmlDocument xmlDocument = new XmlDocument();
            StringWriter xml = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(xml);

            xmlWriter.WriteStartElement("appointment");

            xmlWriter.WriteElementString("doctor_name", doctor);
            xmlWriter.WriteElementString("patient_name", patient);
            xmlWriter.WriteElementString("date", date);

            xmlWriter.WriteEndElement();

            xmlDocument.LoadXml(xml.ToString());

            // Отправляем на сервер
            ArrayList messages = new ArrayList();
            messages.Add("NewAppointment");
            messages.Add(xmlDocument.OuterXml);
            
            this.SendText(messages);

            this.UpdateControls();
        }

        public void CancelAppointment()
        {
            XmlDocument appointmentXml = new XmlDocument();
            StringWriter xml = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("appointment");

            xmlWriter.WriteElementString("visit_id", this.mainView.AppointmentTable.Rows[this.mainView.AppointmentTable.CurrentCell.RowIndex].Cells[0].Value.ToString());

            xmlWriter.WriteEndElement();
            appointmentXml.LoadXml(xml.ToString());

            XmlDocument appointment = this.GetData(appointmentXml);

            if (this.GetValuesFromXml(appointment.OuterXml, "visit_status_id")[0].ToString() != "2")
            {
                this.SendText(new ArrayList(new String[] {"CancelAppointment", appointmentXml.OuterXml}));
                this.UpdateControls();
            }
        }

        public void ResumeAppointment()
        {
            XmlDocument appointmentXml = new XmlDocument();
            StringWriter xml = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("appointment");

            xmlWriter.WriteElementString("visit_id", this.mainView.AppointmentTable.Rows[this.mainView.AppointmentTable.CurrentCell.RowIndex].Cells[0].Value.ToString());

            xmlWriter.WriteEndElement();
            appointmentXml.LoadXml(xml.ToString());

            XmlDocument appointment = this.GetData(appointmentXml);

            if (this.GetValuesFromXml(appointment.OuterXml, "visit_status_id")[0].ToString() != "2")
            {
                this.SendText(new ArrayList(new String[] {"ResumeAppointment", appointmentXml.OuterXml}));
                this.UpdateControls();
            }
        }

        public void NewDoctor()
        {
            Management management = this.mainView.ManagementView;

            String doctorName = management.NewDoctorNameTextBox.Text;
            String doctorDept = management.NewDoctorDeptComboBox.Text;

            XmlDocument deptXml = new XmlDocument();
            StringWriter xml = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("department");
            xmlWriter.WriteElementString("dept_name", doctorDept);
            xmlWriter.WriteEndElement();
            deptXml.LoadXml(xml.ToString());

            deptXml = this.GetData(deptXml);

            String deptId = (String) this.GetValuesFromXml(deptXml.OuterXml, "dept_id")[0];

            DbConnection.Query("insert into doctor values ('" + deptId + "','" + doctorName + "')");

            management.UpdateControls();
        }

        public void NewPatient()
        {
            Management management = this.mainView.ManagementView;

            String patientName = management.NewPatientNameTextBox.Text;
            String patientDocName = management.NewPatientHomeDocComboBox.Text;

            XmlDocument doctorXml = new XmlDocument();
            StringWriter xml = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("doctor");
            xmlWriter.WriteElementString("doctor_name", patientDocName);
            xmlWriter.WriteEndElement();
            doctorXml.LoadXml(xml.ToString());

            doctorXml = this.GetData(doctorXml);
            String doctorId = (String)this.GetValuesFromXml(doctorXml.OuterXml, "doctor_id")[0];

            DbConnection.Query("insert into patient values ('" + patientName + "','" + doctorId + "')");

            management.UpdateControls();
        }

        public void NewDept()
        {
            Management management = this.mainView.ManagementView;

            String deptName = management.NewDeptNameTextBox.Text;

            DbConnection.Query("insert into department values ('"+deptName+"')");

            management.UpdateControls();
        }

        public void Search()
        {
            String input = this.mainView.CleverSearch.Text;

            if (input == "")
            {
                UpdateControls();
            }
            else
            {
                XmlDocument appointmentXml = new XmlDocument();
                StringWriter xml = new StringWriter();
                XmlWriter xmlWriter = new XmlTextWriter(xml);

                XmlDocument doctorXml = new XmlDocument();
                XmlDocument patientXml = new XmlDocument();

                // Здесь будут находиться идентификаторы посещений, которых мы уже отобрали, чтобы не отобрать одинаковые
                ArrayList selectedIds = new ArrayList();

                ArrayList doctorIds = new ArrayList();
                ArrayList patientIds = new ArrayList();
                XmlDocument suitableAppointment = null;
                //ArrayList appointmentIds = new ArrayList();

                // Итоговый документ всех посещений
                XmlDocument appointments = new XmlDocument();

                // Если цифра, то ищем по номеру посещения
                int inputNumeric;
                if (int.TryParse(input, out inputNumeric))
                {
                    xml = new StringWriter();
                    xmlWriter = new XmlTextWriter(xml);
                    xmlWriter.WriteStartElement("appointment");
                    xmlWriter.WriteElementString("visit_id", input);
                    xmlWriter.WriteEndElement();
                    appointmentXml.LoadXml(xml.ToString());

                    suitableAppointment = this.GetData(appointmentXml);
                    appointments = suitableAppointment;
                }
                else
                {
                    // Если не цифра, то ищем по чему-то другому 

                    // Получаем айди по именам
                    xml = new StringWriter();
                    xmlWriter = new XmlTextWriter(xml);
                    xmlWriter.WriteStartElement("doctor");
                    xmlWriter.WriteElementString("doctor_name", input);
                    xmlWriter.WriteEndElement();
                    doctorXml.LoadXml(xml.ToString());
                    doctorXml = this.GetData(doctorXml, true);
                    doctorIds = this.GetValuesFromXml(doctorXml.OuterXml, "doctor_id");

                    xml = new StringWriter();
                    xmlWriter = new XmlTextWriter(xml);
                    xmlWriter.WriteStartElement("patient");
                    xmlWriter.WriteElementString("patient_name", input);
                    xmlWriter.WriteEndElement();
                    patientXml.LoadXml(xml.ToString());
                    patientXml = this.GetData(patientXml, true);
                    patientIds = this.GetValuesFromXml(patientXml.OuterXml, "patient_id");


                    xml = new StringWriter();
                    xmlWriter = new XmlTextWriter(xml);
                    xmlWriter.WriteStartElement("appointment");

                    foreach (String id in doctorIds)
                        xmlWriter.WriteElementString("visit_doctor_id", id);

                    foreach (String id in patientIds)
                        xmlWriter.WriteElementString("visit_patient_id", id);

                    xmlWriter.WriteElementString("visit_date", input);

                    xmlWriter.WriteEndElement();
                    appointmentXml.LoadXml(xml.ToString());

                    
                    appointments = this.GetData(appointmentXml, true);

                }

                // Теперь работаем с таблицей

                this.mainView.AppointmentTable.Rows.Clear();

//                if (suitableAppointment != null || doctorIds.Count > 0 || patientIds.Count > 0)
//                {
                    ArrayList appointmentId = this.GetValuesFromXml(appointments.OuterXml, "visit_id");
                    ArrayList appointmentDoctor = this.GetValuesFromXml(appointments.OuterXml, "visit_doctor_id");
                    ArrayList appointmentPatient = this.GetValuesFromXml(appointments.OuterXml, "visit_patient_id");
                    ArrayList appointmentDate = this.GetValuesFromXml(appointments.OuterXml, "visit_date");
                    ArrayList appointmentStatus = this.GetValuesFromXml(appointments.OuterXml, "visit_status_id");

                    for (var i = 0; i < appointmentId.Count; i++)
                    {
                        // Получаем имена по айди
                        xml = new StringWriter();
                        xmlWriter = new XmlTextWriter(xml);
                        xmlWriter.WriteStartElement("patient");
                        xmlWriter.WriteElementString("patient_id", appointmentPatient[i].ToString());
                        xmlWriter.WriteEndElement();
                        patientXml.LoadXml(xml.ToString());
                        XmlDocument patient = this.GetData(patientXml);
                        String patientName = this.GetValuesFromXml(patient.OuterXml, "patient_name")[0].ToString();

                        xml = new StringWriter();
                        xmlWriter = new XmlTextWriter(xml);
                        xmlWriter.WriteStartElement("doctor");
                        xmlWriter.WriteElementString("doctor_id", appointmentDoctor[i].ToString());
                        xmlWriter.WriteEndElement();
                        doctorXml.LoadXml(xml.ToString());
                        XmlDocument doctor = this.GetData(doctorXml);
                        String doctorName = this.GetValuesFromXml(doctor.OuterXml, "doctor_name")[0].ToString();

                        String[] row =
                            {
                                appointmentId[i].ToString(), doctorName, patientName,
                                appointmentDate[i].ToString()
                            };
                        this.mainView.AppointmentTable.Rows.Add(row);
                        foreach (DataGridViewCell cell in this.mainView.AppointmentTable.Rows[i].Cells)
                        {
                            switch (appointmentStatus[i].ToString())
                            {
                                case "1":
                                    cell.Style.BackColor = Color.LightGoldenrodYellow;
                                    break;
                                case "2":
                                    cell.Style.BackColor = Color.PaleGreen;
                                    break;
                                case "3":
                                    cell.Style.BackColor = Color.LightCoral;
                                    break;
                            }
                        }
                        this.mainView.AppointmentTable.Rows[i].Cells[0].Style.BackColor = Color.LightGoldenrodYellow;
                    }

                    appointmentXml.LoadXml(xml.ToString());
                }
//            }
        }

        public void UpdateControls()
        {
            XmlDocument doctorXml = new XmlDocument();
            XmlDocument patientXml = new XmlDocument();
            XmlDocument appointmentXml = new XmlDocument();

            // doctor, patient и т. д. соответствуют названиям таблиц

            // Создаём пустой xml с докторами (пустой xml означает, что надо вытащить все записи)
            StringWriter xml = new StringWriter();
            XmlWriter xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("doctor");
            //xmlWriter.WriteElementString("Volume", XmlConvert.ToString(10));
            xmlWriter.WriteEndElement();
            doctorXml.LoadXml(xml.ToString());

            // С пациентами
            xml = new StringWriter();
            xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("patient");
            xmlWriter.WriteEndElement();
            patientXml.LoadXml(xml.ToString());

            // С посещенями
            xml = new StringWriter();
            xmlWriter = new XmlTextWriter(xml);
            xmlWriter.WriteStartElement("appointment");
            xmlWriter.WriteEndElement();
            appointmentXml.LoadXml(xml.ToString());

            XmlDocument doctors = this.GetData(doctorXml);
            XmlDocument patients = this.GetData(patientXml);
            XmlDocument appointments = this.GetData(appointmentXml);

            this.mainView.DoctorComboBox1.Items.Clear();
            this.mainView.DoctorComboBox1.Items.AddRange(this.GetValuesFromXml(doctors.OuterXml, "doctor_name").ToArray());
            if (this.mainView.DoctorComboBox1.Items.Count > 0)
                this.mainView.DoctorComboBox1.SelectedIndex = 0;

            this.mainView.PatientComboBox.Items.Clear();
            this.mainView.PatientComboBox.Items.AddRange(this.GetValuesFromXml(patients.OuterXml, "patient_name").ToArray());
            if (this.mainView.PatientComboBox.Items.Count > 0)
                this.mainView.PatientComboBox.SelectedIndex = 0;

            // Теперь работаем с таблицей

            this.mainView.AppointmentTable.Rows.Clear();

            ArrayList appointmentId = this.GetValuesFromXml(appointments.OuterXml, "visit_id");
            ArrayList appointmentDoctor = this.GetValuesFromXml(appointments.OuterXml, "visit_doctor_id");
            ArrayList appointmentPatient = this.GetValuesFromXml(appointments.OuterXml, "visit_patient_id");
            ArrayList appointmentDate = this.GetValuesFromXml(appointments.OuterXml, "visit_date");
            ArrayList appointmentStatus = this.GetValuesFromXml(appointments.OuterXml, "visit_status_id");

            for (var i = 0; i < appointmentId.Count; i++)
            {
                // Получаем имена по айди
                xml = new StringWriter();
                xmlWriter = new XmlTextWriter(xml);
                xmlWriter.WriteStartElement("patient");
                xmlWriter.WriteElementString("patient_id", appointmentPatient[i].ToString());
                xmlWriter.WriteEndElement();
                patientXml.LoadXml(xml.ToString());
                XmlDocument patient = this.GetData(patientXml);
                String patientName = this.GetValuesFromXml(patient.OuterXml, "patient_name")[0].ToString();

                xml = new StringWriter();
                xmlWriter = new XmlTextWriter(xml);
                xmlWriter.WriteStartElement("doctor");
                xmlWriter.WriteElementString("doctor_id", appointmentDoctor[i].ToString());
                xmlWriter.WriteEndElement();
                doctorXml.LoadXml(xml.ToString());
                XmlDocument doctor = this.GetData(doctorXml);
                String doctorName = this.GetValuesFromXml(doctor.OuterXml, "doctor_name")[0].ToString();

                String[] row = { appointmentId[i].ToString(), doctorName, patientName, appointmentDate[i].ToString() };
                this.mainView.AppointmentTable.Rows.Add(row);
                foreach (DataGridViewCell cell in this.mainView.AppointmentTable.Rows[i].Cells)
                {
                    switch (appointmentStatus[i].ToString())
                    {
                        case "1":
                            cell.Style.BackColor = Color.LightGoldenrodYellow;
                            break;
                        case "2":
                            cell.Style.BackColor = Color.PaleGreen;
                            break;
                        case "3":
                            cell.Style.BackColor = Color.LightCoral;
                            break;
                    }
                }
                this.mainView.AppointmentTable.Rows[i].Cells[0].Style.BackColor = Color.LightGoldenrodYellow;
            }

        }


        public XmlDocument GetData(XmlDocument xmlDocument, bool like = false)
        {
            String likeString = "false";
            if (like)
                likeString = "true";
            this.SendText(new ArrayList { "GetData", likeString, xmlDocument.OuterXml });
            String xml = this.GetText();

            xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(xml);

            return xmlDocument;
        }

        private void SendText(ArrayList messages)
        {
            try
            {
                writerStream = tcpClient.GetStream();
                BinaryFormatter format = new BinaryFormatter();

                foreach (var message in messages)
                {
                    format.Serialize(writerStream, message);
                }
            }
            catch (ArgumentException e)
            {
            }
            catch (SocketException e)
            {
            }
        }

        private String GetText()
        {
            NetworkStream readerStream = tcpClient.GetStream();
            BinaryFormatter outformat = new BinaryFormatter();
            String text = outformat.Deserialize(readerStream).ToString();

            return text;
        }

        public ArrayList GetValuesFromXml(String xmlString, String valueName)
        {
            ArrayList values = new ArrayList();

            XmlReader reader = XmlReader.Create(new System.IO.StringReader(xmlString));
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {

                    if (!reader.IsEmptyElement)
                    {
                        if (reader.Name == valueName)
                        {
                            reader.Read();
                            values.Add(reader.ReadString());
                        }
                    }
                }
            }

            return values;
        }

        public Management ManagementView
        {
            get { return managementView; }
            set { managementView = value; }
        }
    }
    

}
