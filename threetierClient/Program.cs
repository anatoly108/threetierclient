﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using threetierClient.client;
using threetierClient.threetier;
using Ini;

namespace threetierClient
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IniFile ini = new IniFile("C:\\threetier.ini");
            String port = ini.IniReadValue("BASIC", "port");
            int portInt;

            if (int.TryParse(port, out portInt))
            {
                IThreetierController threetierController = new ThreetierControllerSurrogate(300);
            }
            else
            {
                MessageBox.Show("Wrong port number in threetier.ini");
            }
            //threetierController.UpdateControls();
        }
    }
}
